/**
 * AEM REST API
 * Autotask Advanced Endpoint Management REST API
 *
 * The version of the OpenAPI document: 2.0.0 (Build 23)
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import JobComponentRequest from './JobComponentRequest';

/**
 * The CreateQuickJobRequest model module.
 * @module model/CreateQuickJobRequest
 * @version 1.0.0
 */
class CreateQuickJobRequest {
    /**
     * Constructs a new <code>CreateQuickJobRequest</code>.
     * @alias module:model/CreateQuickJobRequest
     * @param jobName {String} 
     * @param jobComponent {module:model/JobComponentRequest} 
     */
    constructor(jobName, jobComponent) { 
        
        CreateQuickJobRequest.initialize(this, jobName, jobComponent);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, jobName, jobComponent) { 
        obj['jobName'] = jobName;
        obj['jobComponent'] = jobComponent;
    }

    /**
     * Constructs a <code>CreateQuickJobRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateQuickJobRequest} obj Optional instance to populate.
     * @return {module:model/CreateQuickJobRequest} The populated <code>CreateQuickJobRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CreateQuickJobRequest();

            if (data.hasOwnProperty('jobName')) {
                obj['jobName'] = ApiClient.convertToType(data['jobName'], 'String');
            }
            if (data.hasOwnProperty('jobComponent')) {
                obj['jobComponent'] = JobComponentRequest.constructFromObject(data['jobComponent']);
            }
        }
        return obj;
    }


}

/**
 * @member {String} jobName
 */
CreateQuickJobRequest.prototype['jobName'] = undefined;

/**
 * @member {module:model/JobComponentRequest} jobComponent
 */
CreateQuickJobRequest.prototype['jobComponent'] = undefined;






export default CreateQuickJobRequest;

