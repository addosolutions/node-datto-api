/**
 * AEM REST API
 * Autotask Advanced Endpoint Management REST API
 *
 * The version of the OpenAPI document: 2.0.0 (Build 23)
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The Antivirus model module.
 * @module model/Antivirus
 * @version 1.0.0
 */
class Antivirus {
    /**
     * Constructs a new <code>Antivirus</code>.
     * Device antivirus data
     * @alias module:model/Antivirus
     */
    constructor() { 
        
        Antivirus.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>Antivirus</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/Antivirus} obj Optional instance to populate.
     * @return {module:model/Antivirus} The populated <code>Antivirus</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Antivirus();

            if (data.hasOwnProperty('antivirusProduct')) {
                obj['antivirusProduct'] = ApiClient.convertToType(data['antivirusProduct'], 'String');
            }
            if (data.hasOwnProperty('antivirusStatus')) {
                obj['antivirusStatus'] = ApiClient.convertToType(data['antivirusStatus'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} antivirusProduct
 */
Antivirus.prototype['antivirusProduct'] = undefined;

/**
 * @member {module:model/Antivirus.AntivirusStatusEnum} antivirusStatus
 */
Antivirus.prototype['antivirusStatus'] = undefined;





/**
 * Allowed values for the <code>antivirusStatus</code> property.
 * @enum {String}
 * @readonly
 */
Antivirus['AntivirusStatusEnum'] = {

    /**
     * value: "RunningAndUpToDate"
     * @const
     */
    "RunningAndUpToDate": "RunningAndUpToDate",

    /**
     * value: "RunningAndNotUpToDate"
     * @const
     */
    "RunningAndNotUpToDate": "RunningAndNotUpToDate",

    /**
     * value: "NotRunning"
     * @const
     */
    "NotRunning": "NotRunning",

    /**
     * value: "NotDetected"
     * @const
     */
    "NotDetected": "NotDetected"
};



export default Antivirus;

