/**
 * AEM REST API
 * Autotask Advanced Endpoint Management REST API
 *
 * The version of the OpenAPI document: 2.0.0 (Build 23)
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import AlertMonitorInfo from './AlertMonitorInfo';
import AlertSourceInfo from './AlertSourceInfo';
import ResponseAction from './ResponseAction';

/**
 * The Alert model module.
 * @module model/Alert
 * @version 1.0.0
 */
class Alert {
    /**
     * Constructs a new <code>Alert</code>.
     * Alert data
     * @alias module:model/Alert
     */
    constructor() { 
        
        Alert.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>Alert</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/Alert} obj Optional instance to populate.
     * @return {module:model/Alert} The populated <code>Alert</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Alert();

            if (data.hasOwnProperty('alertContext')) {
                obj['alertContext'] = ApiClient.convertToType(data['alertContext'], Object);
            }
            if (data.hasOwnProperty('alertMonitorInfo')) {
                obj['alertMonitorInfo'] = AlertMonitorInfo.constructFromObject(data['alertMonitorInfo']);
            }
            if (data.hasOwnProperty('alertSourceInfo')) {
                obj['alertSourceInfo'] = AlertSourceInfo.constructFromObject(data['alertSourceInfo']);
            }
            if (data.hasOwnProperty('alertUid')) {
                obj['alertUid'] = ApiClient.convertToType(data['alertUid'], 'String');
            }
            if (data.hasOwnProperty('autoresolveMins')) {
                obj['autoresolveMins'] = ApiClient.convertToType(data['autoresolveMins'], 'Number');
            }
            if (data.hasOwnProperty('diagnostics')) {
                obj['diagnostics'] = ApiClient.convertToType(data['diagnostics'], 'String');
            }
            if (data.hasOwnProperty('muted')) {
                obj['muted'] = ApiClient.convertToType(data['muted'], 'Boolean');
            }
            if (data.hasOwnProperty('priority')) {
                obj['priority'] = ApiClient.convertToType(data['priority'], 'String');
            }
            if (data.hasOwnProperty('resolved')) {
                obj['resolved'] = ApiClient.convertToType(data['resolved'], 'Boolean');
            }
            if (data.hasOwnProperty('resolvedBy')) {
                obj['resolvedBy'] = ApiClient.convertToType(data['resolvedBy'], 'String');
            }
            if (data.hasOwnProperty('resolvedOn')) {
                obj['resolvedOn'] = ApiClient.convertToType(data['resolvedOn'], 'Number');
            }
            if (data.hasOwnProperty('responseActions')) {
                obj['responseActions'] = ApiClient.convertToType(data['responseActions'], [ResponseAction]);
            }
            if (data.hasOwnProperty('ticketNumber')) {
                obj['ticketNumber'] = ApiClient.convertToType(data['ticketNumber'], 'String');
            }
            if (data.hasOwnProperty('timestamp')) {
                obj['timestamp'] = ApiClient.convertToType(data['timestamp'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {Object} alertContext
 */
Alert.prototype['alertContext'] = undefined;

/**
 * @member {module:model/AlertMonitorInfo} alertMonitorInfo
 */
Alert.prototype['alertMonitorInfo'] = undefined;

/**
 * @member {module:model/AlertSourceInfo} alertSourceInfo
 */
Alert.prototype['alertSourceInfo'] = undefined;

/**
 * @member {String} alertUid
 */
Alert.prototype['alertUid'] = undefined;

/**
 * @member {Number} autoresolveMins
 */
Alert.prototype['autoresolveMins'] = undefined;

/**
 * @member {String} diagnostics
 */
Alert.prototype['diagnostics'] = undefined;

/**
 * @member {Boolean} muted
 */
Alert.prototype['muted'] = undefined;

/**
 * @member {module:model/Alert.PriorityEnum} priority
 */
Alert.prototype['priority'] = undefined;

/**
 * @member {Boolean} resolved
 */
Alert.prototype['resolved'] = undefined;

/**
 * @member {String} resolvedBy
 */
Alert.prototype['resolvedBy'] = undefined;

/**
 * @member {Number} resolvedOn
 */
Alert.prototype['resolvedOn'] = undefined;

/**
 * @member {Array.<module:model/ResponseAction>} responseActions
 */
Alert.prototype['responseActions'] = undefined;

/**
 * @member {String} ticketNumber
 */
Alert.prototype['ticketNumber'] = undefined;

/**
 * @member {Number} timestamp
 */
Alert.prototype['timestamp'] = undefined;





/**
 * Allowed values for the <code>priority</code> property.
 * @enum {String}
 * @readonly
 */
Alert['PriorityEnum'] = {

    /**
     * value: "Critical"
     * @const
     */
    "Critical": "Critical",

    /**
     * value: "High"
     * @const
     */
    "High": "High",

    /**
     * value: "Moderate"
     * @const
     */
    "Moderate": "Moderate",

    /**
     * value: "Low"
     * @const
     */
    "Low": "Low",

    /**
     * value: "Information"
     * @const
     */
    "Information": "Information",

    /**
     * value: "Unknown"
     * @const
     */
    "Unknown": "Unknown"
};



export default Alert;

