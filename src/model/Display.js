/**
 * AEM REST API
 * Autotask Advanced Endpoint Management REST API
 *
 * The version of the OpenAPI document: 2.0.0 (Build 23)
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The Display model module.
 * @module model/Display
 * @version 1.0.0
 */
class Display {
    /**
     * Constructs a new <code>Display</code>.
     * Display audit data
     * @alias module:model/Display
     */
    constructor() { 
        
        Display.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>Display</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/Display} obj Optional instance to populate.
     * @return {module:model/Display} The populated <code>Display</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Display();

            if (data.hasOwnProperty('instance')) {
                obj['instance'] = ApiClient.convertToType(data['instance'], 'String');
            }
            if (data.hasOwnProperty('screenHeight')) {
                obj['screenHeight'] = ApiClient.convertToType(data['screenHeight'], 'Number');
            }
            if (data.hasOwnProperty('screenWidth')) {
                obj['screenWidth'] = ApiClient.convertToType(data['screenWidth'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {String} instance
 */
Display.prototype['instance'] = undefined;

/**
 * @member {Number} screenHeight
 */
Display.prototype['screenHeight'] = undefined;

/**
 * @member {Number} screenWidth
 */
Display.prototype['screenWidth'] = undefined;






export default Display;

