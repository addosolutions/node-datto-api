/**
 * AEM REST API
 * Autotask Advanced Endpoint Management REST API
 *
 * The version of the OpenAPI document: 2.0.0 (Build 23)
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The DevicesStatus model module.
 * @module model/DevicesStatus
 * @version 1.0.0
 */
class DevicesStatus {
    /**
     * Constructs a new <code>DevicesStatus</code>.
     * Devices Status
     * @alias module:model/DevicesStatus
     */
    constructor() { 
        
        DevicesStatus.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>DevicesStatus</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/DevicesStatus} obj Optional instance to populate.
     * @return {module:model/DevicesStatus} The populated <code>DevicesStatus</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new DevicesStatus();

            if (data.hasOwnProperty('numberOfDevices')) {
                obj['numberOfDevices'] = ApiClient.convertToType(data['numberOfDevices'], 'Number');
            }
            if (data.hasOwnProperty('numberOfOfflineDevices')) {
                obj['numberOfOfflineDevices'] = ApiClient.convertToType(data['numberOfOfflineDevices'], 'Number');
            }
            if (data.hasOwnProperty('numberOfOnlineDevices')) {
                obj['numberOfOnlineDevices'] = ApiClient.convertToType(data['numberOfOnlineDevices'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {Number} numberOfDevices
 */
DevicesStatus.prototype['numberOfDevices'] = undefined;

/**
 * @member {Number} numberOfOfflineDevices
 */
DevicesStatus.prototype['numberOfOfflineDevices'] = undefined;

/**
 * @member {Number} numberOfOnlineDevices
 */
DevicesStatus.prototype['numberOfOnlineDevices'] = undefined;






export default DevicesStatus;

