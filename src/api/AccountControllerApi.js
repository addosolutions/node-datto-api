/**
 * AEM REST API
 * Autotask Advanced Endpoint Management REST API
 *
 * The version of the OpenAPI document: 2.0.0 (Build 23)
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import Account from '../model/Account';
import AlertsPage from '../model/AlertsPage';
import ComponentsPage from '../model/ComponentsPage';
import DevicesPage from '../model/DevicesPage';
import SitesPage from '../model/SitesPage';
import UsersPage from '../model/UsersPage';

/**
* AccountController service.
* @module api/AccountControllerApi
* @version 1.0.0
*/
export default class AccountControllerApi {

    /**
    * Constructs a new AccountControllerApi. 
    * @alias module:api/AccountControllerApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }



    /**
     * Fetches the components records of the authenticated user's account.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/ComponentsPage} and HTTP response
     */
    getComponentsUsingGETWithHttpInfo(opts) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
        'page': opts['page'],
        'max': opts['max']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = ComponentsPage;
      return this.apiClient.callApi(
        '/v2/account/components', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches the components records of the authenticated user's account.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/ComponentsPage}
     */
    getComponentsUsingGET(opts) {
      return this.getComponentsUsingGETWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches the site records of the authenticated user's account.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/SitesPage} and HTTP response
     */
    getSitesUsingGETWithHttpInfo(opts) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
        'page': opts['page'],
        'max': opts['max']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = SitesPage;
      return this.apiClient.callApi(
        '/v2/account/sites', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches the site records of the authenticated user's account.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/SitesPage}
     */
    getSitesUsingGET(opts) {
      return this.getSitesUsingGETWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches resolved alerts of the authenticated user's account.
     * If the muted parameter is not provided, both muted and umuted alerts will be queried.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @param {Boolean} opts.muted muted
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/AlertsPage} and HTTP response
     */
    getUserAccountClosedAlertsUsingGETWithHttpInfo(opts) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
        'page': opts['page'],
        'max': opts['max'],
        'muted': opts['muted']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = AlertsPage;
      return this.apiClient.callApi(
        '/v2/account/alerts/resolved', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches resolved alerts of the authenticated user's account.
     * If the muted parameter is not provided, both muted and umuted alerts will be queried.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @param {Boolean} opts.muted muted
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AlertsPage}
     */
    getUserAccountClosedAlertsUsingGET(opts) {
      return this.getUserAccountClosedAlertsUsingGETWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches the devices of the authenticated user's account.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/DevicesPage} and HTTP response
     */
    getUserAccountDevicesUsingGETWithHttpInfo(opts) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
        'page': opts['page'],
        'max': opts['max']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = DevicesPage;
      return this.apiClient.callApi(
        '/v2/account/devices', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches the devices of the authenticated user's account.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/DevicesPage}
     */
    getUserAccountDevicesUsingGET(opts) {
      return this.getUserAccountDevicesUsingGETWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches open alerts of the authenticated user's account.
     * If the muted parameter is not provided, both muted and umuted alerts will be queried.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @param {Boolean} opts.muted muted
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/AlertsPage} and HTTP response
     */
    getUserAccountOpenAlertsUsingGETWithHttpInfo(opts) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
        'page': opts['page'],
        'max': opts['max'],
        'muted': opts['muted']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = AlertsPage;
      return this.apiClient.callApi(
        '/v2/account/alerts/open', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches open alerts of the authenticated user's account.
     * If the muted parameter is not provided, both muted and umuted alerts will be queried.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @param {Boolean} opts.muted muted
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AlertsPage}
     */
    getUserAccountOpenAlertsUsingGET(opts) {
      return this.getUserAccountOpenAlertsUsingGETWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches the authenticated user's account data.
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Account} and HTTP response
     */
    getUserAccountUsingGETWithHttpInfo() {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = Account;
      return this.apiClient.callApi(
        '/v2/account', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches the authenticated user's account data.
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Account}
     */
    getUserAccountUsingGET() {
      return this.getUserAccountUsingGETWithHttpInfo()
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches the authentication users records of the authenticated user's account.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/UsersPage} and HTTP response
     */
    getUsersUsingGETWithHttpInfo(opts) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
        'page': opts['page'],
        'max': opts['max']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = UsersPage;
      return this.apiClient.callApi(
        '/v2/account/users', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches the authentication users records of the authenticated user's account.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/UsersPage}
     */
    getUsersUsingGET(opts) {
      return this.getUsersUsingGETWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


}
