/**
 * AEM REST API
 * Autotask Advanced Endpoint Management REST API
 *
 * The version of the OpenAPI document: 2.0.0 (Build 23)
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import AlertsPage from '../model/AlertsPage';
import CreateQuickJobRequest from '../model/CreateQuickJobRequest';
import CreateQuickJobResponse from '../model/CreateQuickJobResponse';
import Device from '../model/Device';
import Udf from '../model/Udf';
import Warranty from '../model/Warranty';

/**
* DeviceController service.
* @module api/DeviceControllerApi
* @version 1.0.0
*/
export default class DeviceControllerApi {

    /**
    * Constructs a new DeviceControllerApi. 
    * @alias module:api/DeviceControllerApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }



    /**
     * Creates a quick job on the device identified by the given device Uid.
     * @param {String} deviceUid deviceUid
     * @param {module:model/CreateQuickJobRequest} createQuickJobRequest createQuickJobRequest
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/CreateQuickJobResponse} and HTTP response
     */
    createQuickJobUsingPUTWithHttpInfo(deviceUid, createQuickJobRequest) {
      let postBody = createQuickJobRequest;
      // verify the required parameter 'deviceUid' is set
      if (deviceUid === undefined || deviceUid === null) {
        throw new Error("Missing the required parameter 'deviceUid' when calling createQuickJobUsingPUT");
      }
      // verify the required parameter 'createQuickJobRequest' is set
      if (createQuickJobRequest === undefined || createQuickJobRequest === null) {
        throw new Error("Missing the required parameter 'createQuickJobRequest' when calling createQuickJobUsingPUT");
      }

      let pathParams = {
        'deviceUid': deviceUid
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = CreateQuickJobResponse;
      return this.apiClient.callApi(
        '/v2/device/{deviceUid}/quickjob', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Creates a quick job on the device identified by the given device Uid.
     * @param {String} deviceUid deviceUid
     * @param {module:model/CreateQuickJobRequest} createQuickJobRequest createQuickJobRequest
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/CreateQuickJobResponse}
     */
    createQuickJobUsingPUT(deviceUid, createQuickJobRequest) {
      return this.createQuickJobUsingPUTWithHttpInfo(deviceUid, createQuickJobRequest)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches data of the device identified by the given device Id.
     * @param {Number} deviceId deviceId
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Device} and HTTP response
     */
    getByIdUsingGETWithHttpInfo(deviceId) {
      let postBody = null;
      // verify the required parameter 'deviceId' is set
      if (deviceId === undefined || deviceId === null) {
        throw new Error("Missing the required parameter 'deviceId' when calling getByIdUsingGET");
      }

      let pathParams = {
        'deviceId': deviceId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = Device;
      return this.apiClient.callApi(
        '/v2/device/id/{deviceId}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches data of the device identified by the given device Id.
     * @param {Number} deviceId deviceId
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Device}
     */
    getByIdUsingGET(deviceId) {
      return this.getByIdUsingGETWithHttpInfo(deviceId)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches data of the device identified by the given device Uid.
     * @param {String} deviceUid deviceUid
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Device} and HTTP response
     */
    getByUidUsingGETWithHttpInfo(deviceUid) {
      let postBody = null;
      // verify the required parameter 'deviceUid' is set
      if (deviceUid === undefined || deviceUid === null) {
        throw new Error("Missing the required parameter 'deviceUid' when calling getByUidUsingGET");
      }

      let pathParams = {
        'deviceUid': deviceUid
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = Device;
      return this.apiClient.callApi(
        '/v2/device/{deviceUid}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches data of the device identified by the given device Uid.
     * @param {String} deviceUid deviceUid
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Device}
     */
    getByUidUsingGET(deviceUid) {
      return this.getByUidUsingGETWithHttpInfo(deviceUid)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches the open alerts of the device identified by the given device Uid.
     * If the muted parameter is not provided, both muted and umuted alerts will be queried.
     * @param {String} deviceUid deviceUid
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @param {Boolean} opts.muted muted
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/AlertsPage} and HTTP response
     */
    getDeviceOpenAlertsUsingGETWithHttpInfo(deviceUid, opts) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'deviceUid' is set
      if (deviceUid === undefined || deviceUid === null) {
        throw new Error("Missing the required parameter 'deviceUid' when calling getDeviceOpenAlertsUsingGET");
      }

      let pathParams = {
        'deviceUid': deviceUid
      };
      let queryParams = {
        'page': opts['page'],
        'max': opts['max'],
        'muted': opts['muted']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = AlertsPage;
      return this.apiClient.callApi(
        '/v2/device/{deviceUid}/alerts/open', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches the open alerts of the device identified by the given device Uid.
     * If the muted parameter is not provided, both muted and umuted alerts will be queried.
     * @param {String} deviceUid deviceUid
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @param {Boolean} opts.muted muted
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AlertsPage}
     */
    getDeviceOpenAlertsUsingGET(deviceUid, opts) {
      return this.getDeviceOpenAlertsUsingGETWithHttpInfo(deviceUid, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches the resolved alerts of the device identified by the given device Uid.
     * If the muted parameter is not provided, both muted and umuted alerts will be queried.
     * @param {String} deviceUid deviceUid
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @param {Boolean} opts.muted muted
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/AlertsPage} and HTTP response
     */
    getDeviceResolvedAlertsUsingGETWithHttpInfo(deviceUid, opts) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'deviceUid' is set
      if (deviceUid === undefined || deviceUid === null) {
        throw new Error("Missing the required parameter 'deviceUid' when calling getDeviceResolvedAlertsUsingGET");
      }

      let pathParams = {
        'deviceUid': deviceUid
      };
      let queryParams = {
        'page': opts['page'],
        'max': opts['max'],
        'muted': opts['muted']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = AlertsPage;
      return this.apiClient.callApi(
        '/v2/device/{deviceUid}/alerts/resolved', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches the resolved alerts of the device identified by the given device Uid.
     * If the muted parameter is not provided, both muted and umuted alerts will be queried.
     * @param {String} deviceUid deviceUid
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page page
     * @param {Number} opts.max max
     * @param {Boolean} opts.muted muted
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AlertsPage}
     */
    getDeviceResolvedAlertsUsingGET(deviceUid, opts) {
      return this.getDeviceResolvedAlertsUsingGETWithHttpInfo(deviceUid, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Moves a device from one site to another site.
     * @param {String} deviceUid The UID of the device to move to a different site
     * @param {String} siteUid The UID of the target site
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing HTTP response
     */
    moveDeviceUsingPUTWithHttpInfo(deviceUid, siteUid) {
      let postBody = null;
      // verify the required parameter 'deviceUid' is set
      if (deviceUid === undefined || deviceUid === null) {
        throw new Error("Missing the required parameter 'deviceUid' when calling moveDeviceUsingPUT");
      }
      // verify the required parameter 'siteUid' is set
      if (siteUid === undefined || siteUid === null) {
        throw new Error("Missing the required parameter 'siteUid' when calling moveDeviceUsingPUT");
      }

      let pathParams = {
        'deviceUid': deviceUid,
        'siteUid': siteUid
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = [];
      let returnType = null;
      return this.apiClient.callApi(
        '/v2/device/{deviceUid}/site/{siteUid}', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Moves a device from one site to another site.
     * @param {String} deviceUid The UID of the device to move to a different site
     * @param {String} siteUid The UID of the target site
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}
     */
    moveDeviceUsingPUT(deviceUid, siteUid) {
      return this.moveDeviceUsingPUTWithHttpInfo(deviceUid, siteUid)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Sets the user defined fields of a device identified by the given device Uid.
     * Any user defined field supplied with an empty value will be set to null. All user defined fields not supplied will retain their current values.
     * @param {String} deviceUid deviceUid
     * @param {module:model/Udf} udf udf
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing HTTP response
     */
    setUdfFieldsUsingPOSTWithHttpInfo(deviceUid, udf) {
      let postBody = udf;
      // verify the required parameter 'deviceUid' is set
      if (deviceUid === undefined || deviceUid === null) {
        throw new Error("Missing the required parameter 'deviceUid' when calling setUdfFieldsUsingPOST");
      }
      // verify the required parameter 'udf' is set
      if (udf === undefined || udf === null) {
        throw new Error("Missing the required parameter 'udf' when calling setUdfFieldsUsingPOST");
      }

      let pathParams = {
        'deviceUid': deviceUid
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = ['application/json'];
      let accepts = [];
      let returnType = null;
      return this.apiClient.callApi(
        '/v2/device/{deviceUid}/udf', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Sets the user defined fields of a device identified by the given device Uid.
     * Any user defined field supplied with an empty value will be set to null. All user defined fields not supplied will retain their current values.
     * @param {String} deviceUid deviceUid
     * @param {module:model/Udf} udf udf
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}
     */
    setUdfFieldsUsingPOST(deviceUid, udf) {
      return this.setUdfFieldsUsingPOSTWithHttpInfo(deviceUid, udf)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Sets the warranty of a device identified by the given device Uid.
     * The warrantyDate field should be a ISO 8601 string with this format: yyyy-mm-dd. The warranty date can also be set to null.
     * @param {String} deviceUid deviceUid
     * @param {module:model/Warranty} warranty warranty
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing HTTP response
     */
    setWarrantyDataUsingPOSTWithHttpInfo(deviceUid, warranty) {
      let postBody = warranty;
      // verify the required parameter 'deviceUid' is set
      if (deviceUid === undefined || deviceUid === null) {
        throw new Error("Missing the required parameter 'deviceUid' when calling setWarrantyDataUsingPOST");
      }
      // verify the required parameter 'warranty' is set
      if (warranty === undefined || warranty === null) {
        throw new Error("Missing the required parameter 'warranty' when calling setWarrantyDataUsingPOST");
      }

      let pathParams = {
        'deviceUid': deviceUid
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = ['application/json'];
      let accepts = [];
      let returnType = null;
      return this.apiClient.callApi(
        '/v2/device/{deviceUid}/warranty', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Sets the warranty of a device identified by the given device Uid.
     * The warrantyDate field should be a ISO 8601 string with this format: yyyy-mm-dd. The warranty date can also be set to null.
     * @param {String} deviceUid deviceUid
     * @param {module:model/Warranty} warranty warranty
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}
     */
    setWarrantyDataUsingPOST(deviceUid, warranty) {
      return this.setWarrantyDataUsingPOSTWithHttpInfo(deviceUid, warranty)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


}
