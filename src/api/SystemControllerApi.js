/**
 * AEM REST API
 * Autotask Advanced Endpoint Management REST API
 *
 * The version of the OpenAPI document: 2.0.0 (Build 23)
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import PaginationConfiguration from '../model/PaginationConfiguration';
import RateStatusResponse from '../model/RateStatusResponse';
import StatusResponse from '../model/StatusResponse';

/**
* SystemController service.
* @module api/SystemControllerApi
* @version 1.0.0
*/
export default class SystemControllerApi {

    /**
    * Constructs a new SystemControllerApi. 
    * @alias module:api/SystemControllerApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }



    /**
     * Fetches the pagination configurations.
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/PaginationConfiguration} and HTTP response
     */
    getPaginationConfigurationsUsingGETWithHttpInfo() {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = PaginationConfiguration;
      return this.apiClient.callApi(
        '/v2/system/pagination', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches the pagination configurations.
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/PaginationConfiguration}
     */
    getPaginationConfigurationsUsingGET() {
      return this.getPaginationConfigurationsUsingGETWithHttpInfo()
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches the system status (start date, status and version).
     * An API access token is not necessary.
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/StatusResponse} and HTTP response
     */
    getStatusUsingGETWithHttpInfo() {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = StatusResponse;
      return this.apiClient.callApi(
        '/v2/system/status', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches the system status (start date, status and version).
     * An API access token is not necessary.
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/StatusResponse}
     */
    getStatusUsingGET() {
      return this.getStatusUsingGETWithHttpInfo()
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Fetches the request rate status for the authenticated user's account.
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/RateStatusResponse} and HTTP response
     */
    getUsingGET1WithHttpInfo() {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = RateStatusResponse;
      return this.apiClient.callApi(
        '/v2/system/request_rate', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Fetches the request rate status for the authenticated user's account.
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/RateStatusResponse}
     */
    getUsingGET1() {
      return this.getUsingGET1WithHttpInfo()
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


}
