/**
 * AEM REST API
 * Autotask Advanced Endpoint Management REST API
 *
 * The version of the OpenAPI document: 2.0.0 (Build 23)
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.datto);
  }
}(this, function(expect, datto) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new datto.AuditControllerApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('AuditControllerApi', function() {
    describe('getDeviceAuditSoftwareUsingGET', function() {
      it('should call getDeviceAuditSoftwareUsingGET successfully', function(done) {
        //uncomment below and update the code to test getDeviceAuditSoftwareUsingGET
        //instance.getDeviceAuditSoftwareUsingGET(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getDeviceAuditUsingGET', function() {
      it('should call getDeviceAuditUsingGET successfully', function(done) {
        //uncomment below and update the code to test getDeviceAuditUsingGET
        //instance.getDeviceAuditUsingGET(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getEsxiHostAuditUsingGET', function() {
      it('should call getEsxiHostAuditUsingGET successfully', function(done) {
        //uncomment below and update the code to test getEsxiHostAuditUsingGET
        //instance.getEsxiHostAuditUsingGET(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getPrinterAuditUsingGET', function() {
      it('should call getPrinterAuditUsingGET successfully', function(done) {
        //uncomment below and update the code to test getPrinterAuditUsingGET
        //instance.getPrinterAuditUsingGET(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
