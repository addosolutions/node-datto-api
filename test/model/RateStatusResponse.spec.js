/**
 * AEM REST API
 * Autotask Advanced Endpoint Management REST API
 *
 * The version of the OpenAPI document: 2.0.0 (Build 23)
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.datto);
  }
}(this, function(expect, datto) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new datto.RateStatusResponse();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('RateStatusResponse', function() {
    it('should create an instance of RateStatusResponse', function() {
      // uncomment below and update the code to test RateStatusResponse
      //var instane = new datto.RateStatusResponse();
      //expect(instance).to.be.a(datto.RateStatusResponse);
    });

    it('should have the property accountCount (base name: "accountCount")', function() {
      // uncomment below and update the code to test the property accountCount
      //var instane = new datto.RateStatusResponse();
      //expect(instance).to.be();
    });

    it('should have the property accountCutOffRatio (base name: "accountCutOffRatio")', function() {
      // uncomment below and update the code to test the property accountCutOffRatio
      //var instane = new datto.RateStatusResponse();
      //expect(instance).to.be();
    });

    it('should have the property accountRateLimit (base name: "accountRateLimit")', function() {
      // uncomment below and update the code to test the property accountRateLimit
      //var instane = new datto.RateStatusResponse();
      //expect(instance).to.be();
    });

    it('should have the property accountUid (base name: "accountUid")', function() {
      // uncomment below and update the code to test the property accountUid
      //var instane = new datto.RateStatusResponse();
      //expect(instance).to.be();
    });

    it('should have the property slidingTimeWindowSizeSeconds (base name: "slidingTimeWindowSizeSeconds")', function() {
      // uncomment below and update the code to test the property slidingTimeWindowSizeSeconds
      //var instane = new datto.RateStatusResponse();
      //expect(instance).to.be();
    });

  });

}));
