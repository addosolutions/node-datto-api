let axios = require("axios");
let {Buffer} = require("buffer");
let DattoApi = require("./dist");
let {ApiClient} = DattoApi

/** @typedef {import('datto-api/src/api/AccountControllerApi')} AccountControllerApi */
/** @typedef {import('datto-api/src/api/AlertsControllerApi')} AlertsControllerApi */
/** @typedef {import('datto-api/src/api/AuditControllerApi')} AuditControllerApi */
/** @typedef {import('datto-api/src/api/DeviceControllerApi')} DeviceControllerApi */
/** @typedef {import('datto-api/src/api/JobControllerApi')} JobControllerApi */
/** @typedef {import('datto-api/src/api/SitesControllerApi')} SitesControllerApi */
/** @typedef {import('datto-api/src/api/SystemControllerApi')} SystemControllerApi */
/** @typedef {import('datto-api/src/api/UserControllerApi')} UserControllerApi */
/** @typedef {import('datto-api/src/model/Device')} Device */
/** @typedef {import('datto-api/src/model/Site')} Site */

/**
 *  @property Account {AccountControllerApi}
 *  @property Alerts {AlertsControllerApi}
 *  @property Audit {AuditControllerApi}
 *  @property Device {DeviceControllerApi}
 *  @property Job {JobControllerApi}
 *  @property Sites {SitesControllerApi}
 *  @property System {SystemControllerApi}
 *  @property User {UserControllerApi}
 */
class Datto {

    /**
     * @param url       {string} Your API URL
     * @param secret    {string} API Secret
     * @param key       {string} API Key
     */
    constructor({url, secret, key}) {
        Object.assign(this, arguments[0]);

        let client = this.client = new ApiClient()
        client.basePath = (this.url.replace(/\/+$/, ''))+"/api";
        for(var i in DattoApi){
            if(i.endsWith('ControllerApi')){
                this[i.substring(0,i.length-13)] = (new (DattoApi[i])(client));
            }
        }
    }

    /**
     * Setup authentication to API endpoint
     * @returns {Promise<void>}
     */
    async connect() {
        this.token = (await axios.post(
            this.url + "auth/oauth/token",
            `grant_type=password&username=${this.key}&password=${this.secret}`,
            {
                headers: {
                    Authorization: "Basic " + Buffer.from("public-client:public", "utf8").toString("base64")
                }
            }
        )).data;

        this.client.applyAuthToRequest=(request)=>{
            request.set({'Authorization': 'Bearer ' + this.token.access_token});
        }

        this.axios = axios.create({
            baseURL: this.url + 'api/v2/',
            timeout: 1000,
            headers: {
                Authorization: "Bearer " + this.token.access_token
            },
            responseType: 'json',
        });

        console.log("[Datto] Acquired Token Successfully!")
    }

    /**
     *
     * @param method
     * @param args
     * @returns {Promise<any>}
     * @private
     */
    _method(method, args) {
        return this.axios[method](...args).then(d => d.data);
    }

    get() {
        return this._method("get", arguments)
    }

    put() {
        return this._method("put", arguments)
    }

    post() {
        return this._method("post", arguments)
    }

    /**
     *  Get's all user deivces
     *
     * @returns {Promise<[Device]>}
     */
    getDevices() {
        return this.Account.getUserAccountDevicesUsingGET().then(d=>d.devices);
    }

    /**
     *  Get's all user deivces
     *
     * @returns {Promise<[Site]>}
     */
    getSites() {
        return this.Account.getSitesUsingGET().then(d=>d.sites);
    }


}

module.exports = {
  ...DattoApi,
  Datto
};
