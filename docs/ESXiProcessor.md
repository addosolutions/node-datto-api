# datto.ESXiProcessor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**frequency** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**numberOfCores** | **Number** |  | [optional] 


