# datto.UsersPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pageDetails** | [**PaginationData**](PaginationData.md) |  | 
**users** | [**[AuthUser]**](AuthUser.md) |  | 


