# datto.SystemControllerApi

All URIs are relative to *http://concord-api.centrastage.net/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPaginationConfigurationsUsingGET**](SystemControllerApi.md#getPaginationConfigurationsUsingGET) | **GET** /v2/system/pagination | Fetches the pagination configurations.
[**getStatusUsingGET**](SystemControllerApi.md#getStatusUsingGET) | **GET** /v2/system/status | Fetches the system status (start date, status and version).
[**getUsingGET1**](SystemControllerApi.md#getUsingGET1) | **GET** /v2/system/request_rate | Fetches the request rate status for the authenticated user&#39;s account.



## getPaginationConfigurationsUsingGET

> PaginationConfiguration getPaginationConfigurationsUsingGET()

Fetches the pagination configurations.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SystemControllerApi();
apiInstance.getPaginationConfigurationsUsingGET().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**PaginationConfiguration**](PaginationConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getStatusUsingGET

> StatusResponse getStatusUsingGET()

Fetches the system status (start date, status and version).

An API access token is not necessary.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SystemControllerApi();
apiInstance.getStatusUsingGET().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUsingGET1

> RateStatusResponse getUsingGET1()

Fetches the request rate status for the authenticated user&#39;s account.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SystemControllerApi();
apiInstance.getUsingGET1().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RateStatusResponse**](RateStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

