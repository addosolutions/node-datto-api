# datto.PrinterAudit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portalUrl** | **String** |  | [optional] 
**printer** | [**Printer**](Printer.md) |  | [optional] 
**printerMarkerSupplies** | [**[PrinterMarkerSupply]**](PrinterMarkerSupply.md) |  | [optional] 
**snmpInfo** | [**SnmpInfo**](SnmpInfo.md) |  | [optional] 
**systemInfo** | [**PrinterSystemInfo**](PrinterSystemInfo.md) |  | [optional] 


