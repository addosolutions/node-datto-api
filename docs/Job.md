# datto.Job

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dateCreated** | **Number** |  | [optional] 
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**uid** | **String** |  | [optional] 



## Enum: StatusEnum


* `active` (value: `"active"`)

* `completed` (value: `"completed"`)




