# datto.DevicesStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberOfDevices** | **Number** |  | [optional] 
**numberOfOfflineDevices** | **Number** |  | [optional] 
**numberOfOnlineDevices** | **Number** |  | [optional] 


