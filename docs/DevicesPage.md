# datto.DevicesPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**devices** | [**[Device]**](Device.md) |  | 
**pageDetails** | [**PaginationData**](PaginationData.md) |  | 


