# datto.JobComponent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**uid** | **String** |  | [optional] 
**variables** | [**[JobComponentVariable]**](JobComponentVariable.md) |  | [optional] 


