# datto.RateStatusResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accountCount** | **Number** |  | [optional] 
**accountCutOffRatio** | **Number** |  | [optional] 
**accountRateLimit** | **Number** |  | [optional] 
**accountUid** | **String** |  | [optional] 
**slidingTimeWindowSizeSeconds** | **Number** |  | [optional] 


