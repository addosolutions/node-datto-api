# datto.SitesControllerApi

All URIs are relative to *http://concord-api.centrastage.net/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUsingPUT**](SitesControllerApi.md#createUsingPUT) | **PUT** /v2/site | Creates a new site in the authenticated user&#39;s account.
[**deleteProxyUsingDELETE**](SitesControllerApi.md#deleteProxyUsingDELETE) | **DELETE** /v2/site/{siteUid}/settings/proxy | Deletes site proxy settings for the site identified by the given site Uid.
[**getSiteDevicesUsingGET**](SitesControllerApi.md#getSiteDevicesUsingGET) | **GET** /v2/site/{siteUid}/devices | Fetches the devices records of the site identified by the given site Uid.
[**getSiteOpenAlertsUsingGET**](SitesControllerApi.md#getSiteOpenAlertsUsingGET) | **GET** /v2/site/{siteUid}/alerts/open | Fetches the open alerts of the site identified by the given site Uid.
[**getSiteResolvedAlertsUsingGET**](SitesControllerApi.md#getSiteResolvedAlertsUsingGET) | **GET** /v2/site/{siteUid}/alerts/resolved | Fetches the resolved alerts of the site identified by the given site Uid.
[**getSiteSettingsUsingGET**](SitesControllerApi.md#getSiteSettingsUsingGET) | **GET** /v2/site/{siteUid}/settings | Fetches settings of the site identified by the given site Uid.
[**getSiteUsingGET**](SitesControllerApi.md#getSiteUsingGET) | **GET** /v2/site/{siteUid} | Fetches data of the site (including total number of devices) identified by the given site Uid.
[**updateProxyUsingPOST**](SitesControllerApi.md#updateProxyUsingPOST) | **POST** /v2/site/{siteUid}/settings/proxy | Creates/updates the proxy settings for the site identified by the given site Uid.
[**updateUsingPOST**](SitesControllerApi.md#updateUsingPOST) | **POST** /v2/site/{siteUid} | Updates the site identified by the given site Uid.



## createUsingPUT

> Site createUsingPUT(createSiteRequest)

Creates a new site in the authenticated user&#39;s account.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SitesControllerApi();
let createSiteRequest = new datto.CreateSiteRequest(); // CreateSiteRequest | createSiteRequest
apiInstance.createUsingPUT(createSiteRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createSiteRequest** | [**CreateSiteRequest**](CreateSiteRequest.md)| createSiteRequest | 

### Return type

[**Site**](Site.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## deleteProxyUsingDELETE

> deleteProxyUsingDELETE(siteUid)

Deletes site proxy settings for the site identified by the given site Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SitesControllerApi();
let siteUid = "siteUid_example"; // String | siteUid
apiInstance.deleteProxyUsingDELETE(siteUid).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **siteUid** | **String**| siteUid | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## getSiteDevicesUsingGET

> DevicesPage getSiteDevicesUsingGET(siteUid, opts)

Fetches the devices records of the site identified by the given site Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SitesControllerApi();
let siteUid = "siteUid_example"; // String | siteUid
let opts = {
  'page': 56, // Number | page
  'max': 56 // Number | max
};
apiInstance.getSiteDevicesUsingGET(siteUid, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **siteUid** | **String**| siteUid | 
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 

### Return type

[**DevicesPage**](DevicesPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSiteOpenAlertsUsingGET

> AlertsPage getSiteOpenAlertsUsingGET(siteUid, opts)

Fetches the open alerts of the site identified by the given site Uid.

If the muted parameter is not provided, both muted and umuted alerts will be queried.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SitesControllerApi();
let siteUid = "siteUid_example"; // String | siteUid
let opts = {
  'page': 56, // Number | page
  'max': 56, // Number | max
  'muted': true // Boolean | muted
};
apiInstance.getSiteOpenAlertsUsingGET(siteUid, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **siteUid** | **String**| siteUid | 
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 
 **muted** | **Boolean**| muted | [optional] 

### Return type

[**AlertsPage**](AlertsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSiteResolvedAlertsUsingGET

> AlertsPage getSiteResolvedAlertsUsingGET(siteUid, opts)

Fetches the resolved alerts of the site identified by the given site Uid.

If the muted parameter is not provided, both muted and umuted alerts will be queried.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SitesControllerApi();
let siteUid = "siteUid_example"; // String | siteUid
let opts = {
  'page': 56, // Number | page
  'max': 56, // Number | max
  'muted': true // Boolean | muted
};
apiInstance.getSiteResolvedAlertsUsingGET(siteUid, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **siteUid** | **String**| siteUid | 
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 
 **muted** | **Boolean**| muted | [optional] 

### Return type

[**AlertsPage**](AlertsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSiteSettingsUsingGET

> SiteSettings getSiteSettingsUsingGET(siteUid)

Fetches settings of the site identified by the given site Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SitesControllerApi();
let siteUid = "siteUid_example"; // String | siteUid
apiInstance.getSiteSettingsUsingGET(siteUid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **siteUid** | **String**| siteUid | 

### Return type

[**SiteSettings**](SiteSettings.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSiteUsingGET

> Site getSiteUsingGET(siteUid)

Fetches data of the site (including total number of devices) identified by the given site Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SitesControllerApi();
let siteUid = "siteUid_example"; // String | siteUid
apiInstance.getSiteUsingGET(siteUid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **siteUid** | **String**| siteUid | 

### Return type

[**Site**](Site.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateProxyUsingPOST

> SiteSettings updateProxyUsingPOST(siteUid, proxySettings)

Creates/updates the proxy settings for the site identified by the given site Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SitesControllerApi();
let siteUid = "siteUid_example"; // String | siteUid
let proxySettings = new datto.ProxySettings(); // ProxySettings | proxySettings
apiInstance.updateProxyUsingPOST(siteUid, proxySettings).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **siteUid** | **String**| siteUid | 
 **proxySettings** | [**ProxySettings**](ProxySettings.md)| proxySettings | 

### Return type

[**SiteSettings**](SiteSettings.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## updateUsingPOST

> Site updateUsingPOST(siteUid, updateSiteRequest)

Updates the site identified by the given site Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.SitesControllerApi();
let siteUid = "siteUid_example"; // String | siteUid
let updateSiteRequest = new datto.SiteRequest(); // SiteRequest | updateSiteRequest
apiInstance.updateUsingPOST(siteUid, updateSiteRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **siteUid** | **String**| siteUid | 
 **updateSiteRequest** | [**SiteRequest**](SiteRequest.md)| updateSiteRequest | 

### Return type

[**Site**](Site.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

