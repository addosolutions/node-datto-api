# datto.PrinterSystemInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manufacturer** | **String** |  | [optional] 
**model** | **String** |  | [optional] 


