# datto.Component

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categoryCode** | **String** |  | [optional] 
**credentialsRequired** | **Boolean** |  | [optional] 
**description** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**uid** | **String** |  | [optional] 
**variables** | [**[ComponentVariable]**](ComponentVariable.md) |  | [optional] 


