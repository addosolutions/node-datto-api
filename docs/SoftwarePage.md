# datto.SoftwarePage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pageDetails** | [**PaginationData**](PaginationData.md) |  | 
**software** | [**[Software]**](Software.md) |  | 


