# datto.ESXiHostAudit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**datastores** | [**[ESXiDatastore]**](ESXiDatastore.md) |  | [optional] 
**guests** | [**[Guest]**](Guest.md) |  | [optional] 
**nics** | [**[ESXiNic]**](ESXiNic.md) |  | [optional] 
**physicalMemory** | [**[PhysicalMemory]**](PhysicalMemory.md) |  | [optional] 
**portalUrl** | **String** |  | [optional] 
**processors** | [**[ESXiProcessor]**](ESXiProcessor.md) |  | [optional] 
**systemInfo** | [**ESXiSystemInfo**](ESXiSystemInfo.md) |  | [optional] 


