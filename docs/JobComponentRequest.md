# datto.JobComponentRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**componentUid** | **String** | Component UID. List of components for the account could be retrieved via account resource | 
**variables** | [**[JobComponentVariable]**](JobComponentVariable.md) |  | [optional] 


