# datto.PatchManagement

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**patchStatus** | **String** |  | [optional] 
**patchesApprovedPending** | **Number** |  | [optional] 
**patchesInstalled** | **Number** |  | [optional] 
**patchesNotApproved** | **Number** |  | [optional] 



## Enum: PatchStatusEnum


* `NoPolicy` (value: `"NoPolicy"`)

* `NoData` (value: `"NoData"`)

* `RebootRequired` (value: `"RebootRequired"`)

* `InstallError` (value: `"InstallError"`)

* `ApprovedPending` (value: `"ApprovedPending"`)

* `FullyPatched` (value: `"FullyPatched"`)




