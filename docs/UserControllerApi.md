# datto.UserControllerApi

All URIs are relative to *http://concord-api.centrastage.net/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**resetApiKeysUsingPOST**](UserControllerApi.md#resetApiKeysUsingPOST) | **POST** /v2/user/resetApiKeys | Resets the authenticated user&#39;s API access and secret keys.



## resetApiKeysUsingPOST

> AuthUserKey resetApiKeysUsingPOST()

Resets the authenticated user&#39;s API access and secret keys.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.UserControllerApi();
apiInstance.resetApiKeysUsingPOST().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**AuthUserKey**](AuthUserKey.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

