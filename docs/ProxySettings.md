# datto.ProxySettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**port** | **Number** |  | [optional] 
**type** | **String** |  | [optional] 
**username** | **String** |  | [optional] 



## Enum: TypeEnum


* `http` (value: `"http"`)

* `socks4` (value: `"socks4"`)

* `socks5` (value: `"socks5"`)




