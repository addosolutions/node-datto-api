# datto.DeviceControllerApi

All URIs are relative to *http://concord-api.centrastage.net/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createQuickJobUsingPUT**](DeviceControllerApi.md#createQuickJobUsingPUT) | **PUT** /v2/device/{deviceUid}/quickjob | Creates a quick job on the device identified by the given device Uid.
[**getByIdUsingGET**](DeviceControllerApi.md#getByIdUsingGET) | **GET** /v2/device/id/{deviceId} | Fetches data of the device identified by the given device Id.
[**getByUidUsingGET**](DeviceControllerApi.md#getByUidUsingGET) | **GET** /v2/device/{deviceUid} | Fetches data of the device identified by the given device Uid.
[**getDeviceOpenAlertsUsingGET**](DeviceControllerApi.md#getDeviceOpenAlertsUsingGET) | **GET** /v2/device/{deviceUid}/alerts/open | Fetches the open alerts of the device identified by the given device Uid.
[**getDeviceResolvedAlertsUsingGET**](DeviceControllerApi.md#getDeviceResolvedAlertsUsingGET) | **GET** /v2/device/{deviceUid}/alerts/resolved | Fetches the resolved alerts of the device identified by the given device Uid.
[**moveDeviceUsingPUT**](DeviceControllerApi.md#moveDeviceUsingPUT) | **PUT** /v2/device/{deviceUid}/site/{siteUid} | Moves a device from one site to another site.
[**setUdfFieldsUsingPOST**](DeviceControllerApi.md#setUdfFieldsUsingPOST) | **POST** /v2/device/{deviceUid}/udf | Sets the user defined fields of a device identified by the given device Uid.
[**setWarrantyDataUsingPOST**](DeviceControllerApi.md#setWarrantyDataUsingPOST) | **POST** /v2/device/{deviceUid}/warranty | Sets the warranty of a device identified by the given device Uid.



## createQuickJobUsingPUT

> CreateQuickJobResponse createQuickJobUsingPUT(deviceUid, createQuickJobRequest)

Creates a quick job on the device identified by the given device Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.DeviceControllerApi();
let deviceUid = "deviceUid_example"; // String | deviceUid
let createQuickJobRequest = new datto.CreateQuickJobRequest(); // CreateQuickJobRequest | createQuickJobRequest
apiInstance.createQuickJobUsingPUT(deviceUid, createQuickJobRequest).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUid** | **String**| deviceUid | 
 **createQuickJobRequest** | [**CreateQuickJobRequest**](CreateQuickJobRequest.md)| createQuickJobRequest | 

### Return type

[**CreateQuickJobResponse**](CreateQuickJobResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getByIdUsingGET

> Device getByIdUsingGET(deviceId)

Fetches data of the device identified by the given device Id.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.DeviceControllerApi();
let deviceId = 789; // Number | deviceId
apiInstance.getByIdUsingGET(deviceId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceId** | **Number**| deviceId | 

### Return type

[**Device**](Device.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getByUidUsingGET

> Device getByUidUsingGET(deviceUid)

Fetches data of the device identified by the given device Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.DeviceControllerApi();
let deviceUid = "deviceUid_example"; // String | deviceUid
apiInstance.getByUidUsingGET(deviceUid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUid** | **String**| deviceUid | 

### Return type

[**Device**](Device.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDeviceOpenAlertsUsingGET

> AlertsPage getDeviceOpenAlertsUsingGET(deviceUid, opts)

Fetches the open alerts of the device identified by the given device Uid.

If the muted parameter is not provided, both muted and umuted alerts will be queried.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.DeviceControllerApi();
let deviceUid = "deviceUid_example"; // String | deviceUid
let opts = {
  'page': 56, // Number | page
  'max': 56, // Number | max
  'muted': true // Boolean | muted
};
apiInstance.getDeviceOpenAlertsUsingGET(deviceUid, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUid** | **String**| deviceUid | 
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 
 **muted** | **Boolean**| muted | [optional] 

### Return type

[**AlertsPage**](AlertsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDeviceResolvedAlertsUsingGET

> AlertsPage getDeviceResolvedAlertsUsingGET(deviceUid, opts)

Fetches the resolved alerts of the device identified by the given device Uid.

If the muted parameter is not provided, both muted and umuted alerts will be queried.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.DeviceControllerApi();
let deviceUid = "deviceUid_example"; // String | deviceUid
let opts = {
  'page': 56, // Number | page
  'max': 56, // Number | max
  'muted': true // Boolean | muted
};
apiInstance.getDeviceResolvedAlertsUsingGET(deviceUid, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUid** | **String**| deviceUid | 
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 
 **muted** | **Boolean**| muted | [optional] 

### Return type

[**AlertsPage**](AlertsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## moveDeviceUsingPUT

> moveDeviceUsingPUT(deviceUid, siteUid)

Moves a device from one site to another site.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.DeviceControllerApi();
let deviceUid = "deviceUid_example"; // String | The UID of the device to move to a different site
let siteUid = "siteUid_example"; // String | The UID of the target site
apiInstance.moveDeviceUsingPUT(deviceUid, siteUid).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUid** | **String**| The UID of the device to move to a different site | 
 **siteUid** | **String**| The UID of the target site | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## setUdfFieldsUsingPOST

> setUdfFieldsUsingPOST(deviceUid, udf)

Sets the user defined fields of a device identified by the given device Uid.

Any user defined field supplied with an empty value will be set to null. All user defined fields not supplied will retain their current values.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.DeviceControllerApi();
let deviceUid = "deviceUid_example"; // String | deviceUid
let udf = new datto.Udf(); // Udf | udf
apiInstance.setUdfFieldsUsingPOST(deviceUid, udf).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUid** | **String**| deviceUid | 
 **udf** | [**Udf**](Udf.md)| udf | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## setWarrantyDataUsingPOST

> setWarrantyDataUsingPOST(deviceUid, warranty)

Sets the warranty of a device identified by the given device Uid.

The warrantyDate field should be a ISO 8601 string with this format: yyyy-mm-dd. The warranty date can also be set to null.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.DeviceControllerApi();
let deviceUid = "deviceUid_example"; // String | deviceUid
let warranty = new datto.Warranty(); // Warranty | warranty
apiInstance.setWarrantyDataUsingPOST(deviceUid, warranty).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUid** | **String**| deviceUid | 
 **warranty** | [**Warranty**](Warranty.md)| warranty | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

