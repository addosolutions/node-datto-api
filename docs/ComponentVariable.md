# datto.ComponentVariable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**defaultVal** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**direction** | **Boolean** |  | [optional] 
**name** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**variablesIdx** | **Number** |  | [optional] 


