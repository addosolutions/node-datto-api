# datto.AuthUserKey

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiAccessKey** | **String** |  | [optional] 
**apiSecretKey** | **String** |  | [optional] 
**userName** | **String** |  | [optional] 


