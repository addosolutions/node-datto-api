# datto.StatusResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**started** | **Number** |  | [optional] 
**status** | **String** |  | [optional] 
**version** | **String** |  | [optional] 


