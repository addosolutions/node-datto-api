# datto.ESXiDatastore

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataExtents** | [**[ESXiDataExtent]**](ESXiDataExtent.md) |  | [optional] 
**datastoreName** | **String** |  | [optional] 
**fileSystem** | **String** |  | [optional] 
**freeSpace** | **Number** |  | [optional] 
**size** | **Number** |  | [optional] 
**status** | **String** |  | [optional] 
**subscriptionPercent** | **Number** |  | [optional] 


