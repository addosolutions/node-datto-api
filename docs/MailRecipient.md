# datto.MailRecipient

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**receivesAlerts** | **Boolean** |  | [optional] 
**receivesNewDevices** | **Boolean** |  | [optional] 
**receivesReports** | **Boolean** |  | [optional] 
**type** | **String** |  | [optional] 


