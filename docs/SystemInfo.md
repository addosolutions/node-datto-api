# datto.SystemInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dotNetVersion** | **String** |  | [optional] 
**manufacturer** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**totalCpuCores** | **Number** |  | [optional] 
**totalPhysicalMemory** | **Number** |  | [optional] 
**username** | **String** |  | [optional] 


