# datto.MobileInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iccid** | **String** |  | [optional] 
**imei** | **String** |  | [optional] 
**_number** | **String** |  | [optional] 
**operator** | **String** |  | [optional] 


