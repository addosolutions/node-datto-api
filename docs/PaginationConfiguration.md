# datto.PaginationConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max** | **Number** | Default and maximum number of elements per AEM page (except alerts pagination) | 


