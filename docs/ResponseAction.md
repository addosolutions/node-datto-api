# datto.ResponseAction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actionReference** | **String** |  | [optional] 
**actionTime** | **Number** |  | [optional] 
**actionType** | **String** |  | [optional] 
**description** | **String** |  | [optional] 



## Enum: ActionTypeEnum


* `EMAIL_SENT` (value: `"EMAIL_SENT"`)

* `EMAIL_SEND_ERROR` (value: `"EMAIL_SEND_ERROR"`)

* `TICKET_PENDING` (value: `"TICKET_PENDING"`)

* `TICKET_CREATED` (value: `"TICKET_CREATED"`)

* `TICKET_CREATION_ERROR` (value: `"TICKET_CREATION_ERROR"`)

* `TICKET_CLOSED_CALL` (value: `"TICKET_CLOSED_CALL"`)




