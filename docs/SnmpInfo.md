# datto.SnmpInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nicManufacturer** | **String** |  | [optional] 
**objectId** | **String** |  | [optional] 
**snmpContact** | **String** |  | [optional] 
**snmpDescription** | **String** |  | [optional] 
**snmpLocation** | **String** |  | [optional] 
**snmpName** | **String** |  | [optional] 
**snmpSerial** | **String** |  | [optional] 
**snmpUptime** | **String** |  | [optional] 


