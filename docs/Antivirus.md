# datto.Antivirus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**antivirusProduct** | **String** |  | [optional] 
**antivirusStatus** | **String** |  | [optional] 



## Enum: AntivirusStatusEnum


* `RunningAndUpToDate` (value: `"RunningAndUpToDate"`)

* `RunningAndNotUpToDate` (value: `"RunningAndNotUpToDate"`)

* `NotRunning` (value: `"NotRunning"`)

* `NotDetected` (value: `"NotDetected"`)




