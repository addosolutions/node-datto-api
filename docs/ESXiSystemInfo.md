# datto.ESXiSystemInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manufacturer** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**numberOfSnapshots** | **Number** |  | [optional] 
**serviceTag** | **String** |  | [optional] 


