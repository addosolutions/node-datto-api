# datto.AlertMonitorInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createsTicket** | **Boolean** |  | [optional] 
**sendsEmails** | **Boolean** |  | [optional] 


