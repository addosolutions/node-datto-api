# datto.AccountDevicesStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numberOfDevices** | **Number** |  | [optional] 
**numberOfManagedDevices** | **Number** |  | [optional] 
**numberOfOfflineDevices** | **Number** |  | [optional] 
**numberOfOnDemandDevices** | **Number** |  | [optional] 
**numberOfOnlineDevices** | **Number** |  | [optional] 


