# datto.AlertSourceInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceName** | **String** |  | [optional] 
**deviceUid** | **String** |  | [optional] 
**siteName** | **String** |  | [optional] 
**siteUid** | **String** |  | [optional] 


