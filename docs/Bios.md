# datto.Bios

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instance** | **String** |  | [optional] 
**releaseDate** | **Number** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**smBiosVersion** | **String** |  | [optional] 


