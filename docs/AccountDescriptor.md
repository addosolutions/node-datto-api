# datto.AccountDescriptor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bilingEmail** | **String** |  | [optional] 
**deviceLimit** | **Number** |  | [optional] 
**timeZone** | **String** |  | [optional] 


