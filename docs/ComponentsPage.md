# datto.ComponentsPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**components** | [**[Component]**](Component.md) |  | 
**pageDetails** | [**PaginationData**](PaginationData.md) |  | 


