# datto.AlertsPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alerts** | [**[Alert]**](Alert.md) |  | 
**pageDetails** | [**PaginationData**](PaginationData.md) |  | 


