# datto.Site

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**autotaskCompanyId** | **String** |  | [optional] 
**autotaskCompanyName** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**devicesStatus** | [**DevicesStatus**](DevicesStatus.md) |  | [optional] 
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**notes** | **String** |  | [optional] 
**onDemand** | **Boolean** |  | [optional] 
**portalUrl** | **String** |  | [optional] 
**proxySettings** | [**ProxySettings**](ProxySettings.md) |  | [optional] 
**splashtopAutoInstall** | **Boolean** |  | [optional] 
**uid** | **String** | Unique alphanumeric UID of this site | [optional] 
**accountUid** | **String** | Unique alphanumeric UID of the account to which this site belongs | [optional] 


