# datto.AttachedDevice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceName** | **String** |  | [optional] 
**deviceType** | **String** |  | [optional] 
**driverFile** | **String** |  | [optional] 
**driverFileLastModified** | **Number** |  | [optional] 
**driverManufacturer** | **String** |  | [optional] 
**driverName** | **String** |  | [optional] 
**driverVersion** | **String** |  | [optional] 
**portName** | **String** |  | [optional] 


