# datto.AlertsControllerApi

All URIs are relative to *http://concord-api.centrastage.net/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAlertUsingGET**](AlertsControllerApi.md#getAlertUsingGET) | **GET** /v2/alert/{alertUid} | Fetches data of the alert identified by the given alert Uid.
[**muteAlertUsingPOST**](AlertsControllerApi.md#muteAlertUsingPOST) | **POST** /v2/alert/{alertUid}/mute | Mutes the alert identified by the given alert Uid.
[**resolveAlertUsingPOST**](AlertsControllerApi.md#resolveAlertUsingPOST) | **POST** /v2/alert/{alertUid}/resolve | Resolves the alert identified by the given alert Uid.
[**unmuteAlertUsingPOST**](AlertsControllerApi.md#unmuteAlertUsingPOST) | **POST** /v2/alert/{alertUid}/unmute | Unmutes the alert identified by the given alert Uid.



## getAlertUsingGET

> Alert getAlertUsingGET(alertUid)

Fetches data of the alert identified by the given alert Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AlertsControllerApi();
let alertUid = "alertUid_example"; // String | alertUid
apiInstance.getAlertUsingGET(alertUid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alertUid** | **String**| alertUid | 

### Return type

[**Alert**](Alert.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## muteAlertUsingPOST

> muteAlertUsingPOST(alertUid)

Mutes the alert identified by the given alert Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AlertsControllerApi();
let alertUid = "alertUid_example"; // String | alertUid
apiInstance.muteAlertUsingPOST(alertUid).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alertUid** | **String**| alertUid | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## resolveAlertUsingPOST

> resolveAlertUsingPOST(alertUid)

Resolves the alert identified by the given alert Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AlertsControllerApi();
let alertUid = "alertUid_example"; // String | alertUid
apiInstance.resolveAlertUsingPOST(alertUid).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alertUid** | **String**| alertUid | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## unmuteAlertUsingPOST

> unmuteAlertUsingPOST(alertUid)

Unmutes the alert identified by the given alert Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AlertsControllerApi();
let alertUid = "alertUid_example"; // String | alertUid
apiInstance.unmuteAlertUsingPOST(alertUid).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alertUid** | **String**| alertUid | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

