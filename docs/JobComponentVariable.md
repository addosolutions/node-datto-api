# datto.JobComponentVariable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Variable name has to match one of component&#39;s variables | 
**value** | **String** | Value has to be convertible to the type specified in component variable | 


