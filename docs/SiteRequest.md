# datto.SiteRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  | [optional] 
**name** | **String** |  | 
**notes** | **String** |  | [optional] 
**onDemand** | **Boolean** |  | [optional] 
**splashtopAutoInstall** | **Boolean** |  | [optional] 


