# datto.Alert

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alertContext** | [**Object**](.md) |  | [optional] 
**alertMonitorInfo** | [**AlertMonitorInfo**](AlertMonitorInfo.md) |  | [optional] 
**alertSourceInfo** | [**AlertSourceInfo**](AlertSourceInfo.md) |  | [optional] 
**alertUid** | **String** |  | [optional] 
**autoresolveMins** | **Number** |  | [optional] 
**diagnostics** | **String** |  | [optional] 
**muted** | **Boolean** |  | [optional] 
**priority** | **String** |  | [optional] 
**resolved** | **Boolean** |  | [optional] 
**resolvedBy** | **String** |  | [optional] 
**resolvedOn** | **Number** |  | [optional] 
**responseActions** | [**[ResponseAction]**](ResponseAction.md) |  | [optional] 
**ticketNumber** | **String** |  | [optional] 
**timestamp** | **Number** |  | [optional] 



## Enum: PriorityEnum


* `Critical` (value: `"Critical"`)

* `High` (value: `"High"`)

* `Moderate` (value: `"Moderate"`)

* `Low` (value: `"Low"`)

* `Information` (value: `"Information"`)

* `Unknown` (value: `"Unknown"`)




