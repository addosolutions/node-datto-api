# datto.Udf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**udf1** | **String** |  | [optional] 
**udf10** | **String** |  | [optional] 
**udf11** | **String** |  | [optional] 
**udf12** | **String** |  | [optional] 
**udf13** | **String** |  | [optional] 
**udf14** | **String** |  | [optional] 
**udf15** | **String** |  | [optional] 
**udf16** | **String** |  | [optional] 
**udf17** | **String** |  | [optional] 
**udf18** | **String** |  | [optional] 
**udf19** | **String** |  | [optional] 
**udf2** | **String** |  | [optional] 
**udf20** | **String** |  | [optional] 
**udf21** | **String** |  | [optional] 
**udf22** | **String** |  | [optional] 
**udf23** | **String** |  | [optional] 
**udf24** | **String** |  | [optional] 
**udf25** | **String** |  | [optional] 
**udf26** | **String** |  | [optional] 
**udf27** | **String** |  | [optional] 
**udf28** | **String** |  | [optional] 
**udf29** | **String** |  | [optional] 
**udf3** | **String** |  | [optional] 
**udf30** | **String** |  | [optional] 
**udf4** | **String** |  | [optional] 
**udf5** | **String** |  | [optional] 
**udf6** | **String** |  | [optional] 
**udf7** | **String** |  | [optional] 
**udf8** | **String** |  | [optional] 
**udf9** | **String** |  | [optional] 


