# datto.PaginationData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Number** |  | [optional] 
**nextPageUrl** | **String** |  | [optional] 
**prevPageUrl** | **String** |  | [optional] 


