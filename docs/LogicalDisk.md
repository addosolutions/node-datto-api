# datto.LogicalDisk

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  | [optional] 
**diskIdentifier** | **String** |  | [optional] 
**freespace** | **Number** |  | [optional] 
**size** | **Number** |  | [optional] 


