# datto.AuthUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **Number** |  | [optional] 
**disabled** | **Boolean** |  | [optional] 
**email** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**lastAccess** | **Number** |  | [optional] 
**lastName** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**telephone** | **String** |  | [optional] 
**username** | **String** |  | [optional] 


