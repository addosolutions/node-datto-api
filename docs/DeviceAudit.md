# datto.DeviceAudit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attachedDevices** | [**[AttachedDevice]**](AttachedDevice.md) |  | [optional] 
**baseBoard** | [**BaseBoard**](BaseBoard.md) |  | [optional] 
**bios** | [**Bios**](Bios.md) |  | [optional] 
**displays** | [**[Display]**](Display.md) |  | [optional] 
**logicalDisks** | [**[LogicalDisk]**](LogicalDisk.md) |  | [optional] 
**mobileInfo** | [**[MobileInfo]**](MobileInfo.md) |  | [optional] 
**nics** | [**[NetworkInterface]**](NetworkInterface.md) |  | [optional] 
**physicalMemory** | [**[PhysicalMemory]**](PhysicalMemory.md) |  | [optional] 
**portalUrl** | **String** |  | [optional] 
**processors** | [**[Processor]**](Processor.md) |  | [optional] 
**snmpInfo** | [**SnmpInfo**](SnmpInfo.md) |  | [optional] 
**systemInfo** | [**SystemInfo**](SystemInfo.md) |  | [optional] 
**videoBoards** | [**[VideoBoard]**](VideoBoard.md) |  | [optional] 


