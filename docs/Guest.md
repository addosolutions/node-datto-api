# datto.Guest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**datastores** | **String** |  | [optional] 
**guestName** | **String** |  | [optional] 
**memorySizeTotal** | **Number** |  | [optional] 
**numberOfSnapshots** | **Number** |  | [optional] 
**processorSpeedTotal** | **Number** |  | [optional] 


