# datto.AuditControllerApi

All URIs are relative to *http://concord-api.centrastage.net/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDeviceAuditSoftwareUsingGET**](AuditControllerApi.md#getDeviceAuditSoftwareUsingGET) | **GET** /v2/audit/device/{deviceUid}/software | Fetches audited software of the generic device identified the given device Uid.
[**getDeviceAuditUsingGET**](AuditControllerApi.md#getDeviceAuditUsingGET) | **GET** /v2/audit/device/{deviceUid} | Fetches audit data of the generic device identified the given device Uid.
[**getEsxiHostAuditUsingGET**](AuditControllerApi.md#getEsxiHostAuditUsingGET) | **GET** /v2/audit/esxihost/{deviceUid} | Fetches audit data of the ESXi host identified the given device Uid.
[**getPrinterAuditUsingGET**](AuditControllerApi.md#getPrinterAuditUsingGET) | **GET** /v2/audit/printer/{deviceUid} | Fetches audit data of the printer identified the given device Uid.



## getDeviceAuditSoftwareUsingGET

> SoftwarePage getDeviceAuditSoftwareUsingGET(deviceUid, opts)

Fetches audited software of the generic device identified the given device Uid.

The device class must be of type \&quot;device\&quot;.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AuditControllerApi();
let deviceUid = "deviceUid_example"; // String | deviceUid
let opts = {
  'page': 56, // Number | page
  'max': 56 // Number | max
};
apiInstance.getDeviceAuditSoftwareUsingGET(deviceUid, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUid** | **String**| deviceUid | 
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 

### Return type

[**SoftwarePage**](SoftwarePage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDeviceAuditUsingGET

> DeviceAudit getDeviceAuditUsingGET(deviceUid)

Fetches audit data of the generic device identified the given device Uid.

The device class must be of type \&quot;device\&quot;.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AuditControllerApi();
let deviceUid = "deviceUid_example"; // String | deviceUid
apiInstance.getDeviceAuditUsingGET(deviceUid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUid** | **String**| deviceUid | 

### Return type

[**DeviceAudit**](DeviceAudit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getEsxiHostAuditUsingGET

> ESXiHostAudit getEsxiHostAuditUsingGET(deviceUid)

Fetches audit data of the ESXi host identified the given device Uid.

The device class must be of type \&quot;esxihost\&quot;.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AuditControllerApi();
let deviceUid = "deviceUid_example"; // String | deviceUid
apiInstance.getEsxiHostAuditUsingGET(deviceUid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUid** | **String**| deviceUid | 

### Return type

[**ESXiHostAudit**](ESXiHostAudit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPrinterAuditUsingGET

> PrinterAudit getPrinterAuditUsingGET(deviceUid)

Fetches audit data of the printer identified the given device Uid.

The device class must be of type \&quot;printer\&quot;.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AuditControllerApi();
let deviceUid = "deviceUid_example"; // String | deviceUid
apiInstance.getPrinterAuditUsingGET(deviceUid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUid** | **String**| deviceUid | 

### Return type

[**PrinterAudit**](PrinterAudit.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

