# datto.PhysicalMemory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bank** | **String** |  | [optional] 
**module** | **String** |  | [optional] 
**partNumber** | **String** |  | [optional] 
**serialNumber** | **String** |  | [optional] 
**size** | **Number** |  | [optional] 
**speed** | **String** |  | [optional] 
**type** | **String** |  | [optional] 


