# datto.ESXiNic

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ipv4** | **String** |  | [optional] 
**ipv6** | **String** |  | [optional] 
**macAddress** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**speed** | **String** |  | [optional] 


