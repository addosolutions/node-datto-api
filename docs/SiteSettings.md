# datto.SiteSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**generalSettings** | [**GeneralSettings**](GeneralSettings.md) |  | [optional] 
**mailRecipients** | [**[MailRecipient]**](MailRecipient.md) |  | [optional] 
**proxySettings** | [**ProxySettings**](ProxySettings.md) |  | [optional] 


