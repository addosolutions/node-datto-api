# datto.JobControllerApi

All URIs are relative to *http://concord-api.centrastage.net/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getJobComponentsUsingGET**](JobControllerApi.md#getJobComponentsUsingGET) | **GET** /v2/job/{jobUid}/components | Fetches components of the job identified by the given job Uid.
[**getUsingGET**](JobControllerApi.md#getUsingGET) | **GET** /v2/job/{jobUid} | Fetches data of the job identified by the given job Uid.



## getJobComponentsUsingGET

> JobComponentsPage getJobComponentsUsingGET(jobUid, opts)

Fetches components of the job identified by the given job Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.JobControllerApi();
let jobUid = "jobUid_example"; // String | jobUid
let opts = {
  'page': 56, // Number | page
  'max': 56 // Number | max
};
apiInstance.getJobComponentsUsingGET(jobUid, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobUid** | **String**| jobUid | 
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 

### Return type

[**JobComponentsPage**](JobComponentsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUsingGET

> Job getUsingGET(jobUid)

Fetches data of the job identified by the given job Uid.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.JobControllerApi();
let jobUid = "jobUid_example"; // String | jobUid
apiInstance.getUsingGET(jobUid).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobUid** | **String**| jobUid | 

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

