# datto.Account

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **String** |  | [optional] 
**descriptor** | [**AccountDescriptor**](AccountDescriptor.md) |  | [optional] 
**devicesStatus** | [**AccountDevicesStatus**](AccountDevicesStatus.md) |  | [optional] 
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**uid** | **String** |  | [optional] 


