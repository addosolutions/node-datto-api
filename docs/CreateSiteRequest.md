# datto.CreateSiteRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  | [optional] 
**name** | **String** |  | 
**notes** | **String** |  | [optional] 
**onDemand** | **Boolean** |  | [optional] 
**proxySettings** | [**ProxySettings**](ProxySettings.md) |  | [optional] 
**splashtopAutoInstall** | **Boolean** |  | [optional] 


