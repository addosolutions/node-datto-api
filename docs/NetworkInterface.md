# datto.NetworkInterface

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instance** | **String** |  | [optional] 
**ipv4** | **String** |  | [optional] 
**ipv6** | **String** |  | [optional] 
**macAddress** | **String** |  | [optional] 


