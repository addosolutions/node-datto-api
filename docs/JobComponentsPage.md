# datto.JobComponentsPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jobComponents** | [**[JobComponent]**](JobComponent.md) |  | 
**pageDetails** | [**PaginationData**](PaginationData.md) |  | 


