# datto.ESXiDataExtent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mediaType** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**size** | **Number** |  | [optional] 
**status** | **String** |  | [optional] 


