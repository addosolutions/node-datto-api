# datto.CreateQuickJobResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job** | [**Job**](Job.md) |  | [optional] 
**jobComponents** | [**[JobComponent]**](JobComponent.md) |  | [optional] 


