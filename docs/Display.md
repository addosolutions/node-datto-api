# datto.Display

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instance** | **String** |  | [optional] 
**screenHeight** | **Number** |  | [optional] 
**screenWidth** | **Number** |  | [optional] 


