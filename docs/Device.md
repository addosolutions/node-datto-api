# datto.Device

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**a64Bit** | **Boolean** |  | [optional] 
**antivirus** | [**Antivirus**](Antivirus.md) |  | [optional] 
**cagVersion** | **String** |  | [optional] 
**creationDate** | **Number** |  | [optional] 
**deleted** | **Boolean** |  | [optional] 
**description** | **String** |  | [optional] 
**deviceClass** | **String** |  | [optional] 
**deviceType** | [**DevicesType**](DevicesType.md) |  | [optional] 
**displayVersion** | **String** |  | [optional] 
**domain** | **String** |  | [optional] 
**extIpAddress** | **String** |  | [optional] 
**hostname** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 
**intIpAddress** | **String** |  | [optional] 
**lastAuditDate** | **Number** |  | [optional] 
**lastLoggedInUser** | **String** |  | [optional] 
**lastReboot** | **Number** |  | [optional] 
**lastSeen** | **Number** |  | [optional] 
**online** | **Boolean** |  | [optional] 
**operatingSystem** | **String** |  | [optional] 
**patchManagement** | [**PatchManagement**](PatchManagement.md) |  | [optional] 
**portalUrl** | **String** |  | [optional] 
**rebootRequired** | **Boolean** |  | [optional] 
**siteId** | **Number** |  | [optional] 
**siteName** | **String** |  | [optional] 
**siteUid** | **String** |  | [optional] 
**snmpEnabled** | **Boolean** |  | [optional] 
**softwareStatus** | **String** |  | [optional] 
**suspended** | **Boolean** |  | [optional] 
**udf** | [**Udf**](Udf.md) |  | [optional] 
**uid** | **String** |  | [optional] 
**warrantyDate** | **String** |  | [optional] 



## Enum: DeviceClassEnum


* `device` (value: `"device"`)

* `printer` (value: `"printer"`)

* `esxihost` (value: `"esxihost"`)

* `unknown` (value: `"unknown"`)




