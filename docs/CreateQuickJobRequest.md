# datto.CreateQuickJobRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jobName** | **String** |  | 
**jobComponent** | [**JobComponentRequest**](JobComponentRequest.md) |  | 


