# datto.AccountControllerApi

All URIs are relative to *http://concord-api.centrastage.net/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getComponentsUsingGET**](AccountControllerApi.md#getComponentsUsingGET) | **GET** /v2/account/components | Fetches the components records of the authenticated user&#39;s account.
[**getSitesUsingGET**](AccountControllerApi.md#getSitesUsingGET) | **GET** /v2/account/sites | Fetches the site records of the authenticated user&#39;s account.
[**getUserAccountClosedAlertsUsingGET**](AccountControllerApi.md#getUserAccountClosedAlertsUsingGET) | **GET** /v2/account/alerts/resolved | Fetches resolved alerts of the authenticated user&#39;s account.
[**getUserAccountDevicesUsingGET**](AccountControllerApi.md#getUserAccountDevicesUsingGET) | **GET** /v2/account/devices | Fetches the devices of the authenticated user&#39;s account.
[**getUserAccountOpenAlertsUsingGET**](AccountControllerApi.md#getUserAccountOpenAlertsUsingGET) | **GET** /v2/account/alerts/open | Fetches open alerts of the authenticated user&#39;s account.
[**getUserAccountUsingGET**](AccountControllerApi.md#getUserAccountUsingGET) | **GET** /v2/account | Fetches the authenticated user&#39;s account data.
[**getUsersUsingGET**](AccountControllerApi.md#getUsersUsingGET) | **GET** /v2/account/users | Fetches the authentication users records of the authenticated user&#39;s account.



## getComponentsUsingGET

> ComponentsPage getComponentsUsingGET(opts)

Fetches the components records of the authenticated user&#39;s account.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AccountControllerApi();
let opts = {
  'page': 56, // Number | page
  'max': 56 // Number | max
};
apiInstance.getComponentsUsingGET(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 

### Return type

[**ComponentsPage**](ComponentsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSitesUsingGET

> SitesPage getSitesUsingGET(opts)

Fetches the site records of the authenticated user&#39;s account.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AccountControllerApi();
let opts = {
  'page': 56, // Number | page
  'max': 56 // Number | max
};
apiInstance.getSitesUsingGET(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 

### Return type

[**SitesPage**](SitesPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUserAccountClosedAlertsUsingGET

> AlertsPage getUserAccountClosedAlertsUsingGET(opts)

Fetches resolved alerts of the authenticated user&#39;s account.

If the muted parameter is not provided, both muted and umuted alerts will be queried.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AccountControllerApi();
let opts = {
  'page': 56, // Number | page
  'max': 56, // Number | max
  'muted': true // Boolean | muted
};
apiInstance.getUserAccountClosedAlertsUsingGET(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 
 **muted** | **Boolean**| muted | [optional] 

### Return type

[**AlertsPage**](AlertsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUserAccountDevicesUsingGET

> DevicesPage getUserAccountDevicesUsingGET(opts)

Fetches the devices of the authenticated user&#39;s account.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AccountControllerApi();
let opts = {
  'page': 56, // Number | page
  'max': 56 // Number | max
};
apiInstance.getUserAccountDevicesUsingGET(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 

### Return type

[**DevicesPage**](DevicesPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUserAccountOpenAlertsUsingGET

> AlertsPage getUserAccountOpenAlertsUsingGET(opts)

Fetches open alerts of the authenticated user&#39;s account.

If the muted parameter is not provided, both muted and umuted alerts will be queried.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AccountControllerApi();
let opts = {
  'page': 56, // Number | page
  'max': 56, // Number | max
  'muted': true // Boolean | muted
};
apiInstance.getUserAccountOpenAlertsUsingGET(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 
 **muted** | **Boolean**| muted | [optional] 

### Return type

[**AlertsPage**](AlertsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUserAccountUsingGET

> Account getUserAccountUsingGET()

Fetches the authenticated user&#39;s account data.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AccountControllerApi();
apiInstance.getUserAccountUsingGET().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUsersUsingGET

> UsersPage getUsersUsingGET(opts)

Fetches the authentication users records of the authenticated user&#39;s account.

### Example

```javascript
import datto from '@addosolutions/datto-api';

let apiInstance = new datto.AccountControllerApi();
let opts = {
  'page': 56, // Number | page
  'max': 56 // Number | max
};
apiInstance.getUsersUsingGET(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| page | [optional] 
 **max** | **Number**| max | [optional] 

### Return type

[**UsersPage**](UsersPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

