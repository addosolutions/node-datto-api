# datto.PrinterMarkerSupply

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  | [optional] 
**maxCapacity** | **String** |  | [optional] 
**suppliesLevel** | **String** |  | [optional] 


