# datto.SitesPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pageDetails** | [**PaginationData**](PaginationData.md) |  | 
**sites** | [**[Site]**](Site.md) |  | 


